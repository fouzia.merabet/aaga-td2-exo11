import java.util.Random;

public class main {

	public static void main(String[] args) {
		Noeud racine = null;
		Arbre abr = new Arbre();
		final int SIZE = 400;
		int[] a = new int[SIZE];
		Random random = new Random();
		for (int i = 0; i < SIZE; i++) {
			a[i] = random.nextInt(400);
		}
		for (int i = 0; i < SIZE; i++) {
			racine = abr.add(racine, a[i]);
		}
		abr.avancer(racine);

	}

}
