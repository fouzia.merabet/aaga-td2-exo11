
public class Arbre {
	public Noeud add(Noeud racine, int v) {
        if (racine == null) {
			racine = new Noeud();
			racine.v = v;
		} else if (v < racine.v) {
			racine.fils_g = add(racine.fils_g, v);
		} else {
			racine.fils_d = add(racine.fils_d, v);
		}
		return racine;
    }

    public void avancer(Noeud racine) {
        if (racine == null) {
            return;
        }
        avancer(racine.fils_g);
        avancer(racine.fils_d);
    }

}
